FROM registry.gitlab.com/xdpedrox/ruby-node/master:latest

COPY src /app

WORKDIR /app

RUN bundle install 
# --deployment --without development test

RUN yarn

# RUN bundle exec rails test

# RUN bundle exec rake assets:precompile 


# # CMD [ "-D", "FOREGROUND" ]
# # ENTRYPOINT ["bundle"]
# CMD ["rails s"]

# RUN rm .bundle/config
# RAILS_ENV=production
