
// Libraries
import react from 'react';

import ColorPicker from './ColorPicker';

function app(props) {
    return (<ColorPicker {...props}/>);
}
 

export default app;

