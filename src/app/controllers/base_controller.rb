class BaseController < ActionController::Base

  rescue_from ActionController::UnknownFormat, with: :unknown_format

  layout "application"
    
  helper :all

  private

  def unknown_format(exception)
    render plain: 'Unsupported Format', status: 406
  end
  
end
